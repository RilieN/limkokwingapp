package com.vm.limkokwing;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import info.hoang8f.widget.FButton;


public class Limkokwing extends ActionBarActivity {
    FButton curse,about,contact_us;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.limkokwing);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.on_click);

        curse = (FButton)findViewById(R.id.curse);
        curse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next1 = new Intent(Limkokwing.this,MainActivity.class);
                mp.start();
                startActivity(next1);
                finish();
            }
        });

        about= (FButton)findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next1 = new Intent(Limkokwing.this,About_us.class);
                mp.start();
                startActivity(next1);
                finish();
            }
        });

        contact_us= (FButton)findViewById(R.id.contact);
        contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next1 = new Intent(Limkokwing.this,Contact_us.class);
                mp.start();
                startActivity(next1);
                finish();
            }
        });
    }
}