package com.vm.limkokwing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.melnykov.fab.FloatingActionButton;


public class About_us extends ActionBarActivity {
    FloatingActionButton note_but;
    TextView tx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        VideoView mVideoView = (VideoView)findViewById(R.id.limkokwingVideo);

        String uriPath = "android.resource://com.vm.limkokwing/"+R.raw.limkokwing;
        Uri uri = Uri.parse(uriPath);
        mVideoView.setVideoURI(uri);
        mVideoView.start();

        tx =(TextView)findViewById(R.id.text_id);
        note_but = (FloatingActionButton)findViewById(R.id.fab_about);
        note_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(About_us.this)
                        .items(R.array.preference_values)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                if(which == 0){
                            tx.setText(R.string.the_chancellor);
                                }
                                if(which == 1){
                                    tx.setText(R.string.limkokwing_university);
                                }
                                if(which == 2){
                                    tx.setText(R.string.fict);
                                }

                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(About_us.this,Limkokwing.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}