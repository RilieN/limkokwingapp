package com.vm.limkokwing;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.cards.BasicButtonsCard;
import com.dexafree.materialList.cards.OnButtonPressListener;
import com.dexafree.materialList.cards.SimpleCard;
import com.dexafree.materialList.model.Card;
import com.dexafree.materialList.view.MaterialListView;


public class MainActivity extends ActionBarActivity {
    private Context mContext;
    private MaterialListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        mListView = (MaterialListView) findViewById(R.id.material_listview);

        fillArray();
    }

    private void fillArray() {
        for (int i = 0; i < 10; i++) {
            Card card = getCard(i);
            mListView.add(card);
        }
    }

    private Card getCard(final int position) {
        SimpleCard card;

        switch (position) {
            case 0:
                card = new BasicButtonsCard(this);
                card.setDescription("Business intelligence (BI) is a broad category of applications and technologies for gathering, storing, analyzing, and providing access to data to ...");
                card.setTitle("Bachelor of Science (Hons) in Business Intelligence System");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section1)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page1 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_business_intelligence_system"));
                        startActivity(page1);
                    }
                });
                card.setDismissible(false);

                return card;

            case 1:
                card = new BasicButtonsCard(this);
                card.setDescription("Cloud computing is the technology of storing and accessing data and programs over the Internet instead of hard drive in the computer. A research conducted by IBM indicated ...");
                card.setTitle("Bachelor of Computer Science (Hons) in Cloud Computing");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section2)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_computer_science_hons_in_cloud_computing"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 2:
                card = new BasicButtonsCard(this);
                card.setDescription("This programme is designed to equip and provide students with a thorough grounding in the area of modern business approaches with the use of ...");
                card.setTitle("Bachelor of Science (Hons) in Business Information Technology");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section3)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_business_information_technology"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 3:
                card = new BasicButtonsCard(this);
                card.setDescription("Programme aims to equip students with updated knowledge and skills in both the technical and business aspects of electronic COMMERCE...");
                card.setTitle("Bachelor of Science (Hons) in Electronic Commerce");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }
                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section4)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_electronic_commerce"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 4:
                card = new BasicButtonsCard(this);
                card.setDescription("Informatics is the transformation of information, which is the pivotal function of any computer system. Visual computing, on the other hand, is about ...");
                card.setTitle("Bachelor of Science (Hons) in Informatics & Visual Computing");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section5)
                                .positiveText("DONE")
                                .show();
                }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_informatics_visual_computing"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 5:
                card = new BasicButtonsCard(this);
                card.setDescription("Like the internet, network applications have made a tremendous impact on industry, COMMERCE, education and society. The students will have the opportunity to develop ...");
                card.setTitle("Bachelor of Science (Hons) in Information and Communication Technology");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section6)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_information_and_communication_technology"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 6:
                card = new BasicButtonsCard(this);
                card.setDescription("A broad-based programme of studies, which aims to acquaint students with a wide range of problems that arise in computing & information technology, together with ...");
                card.setTitle("Bachelor of Science (Hons) in Information Technology");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }


                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section7)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_information_technology"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

           case 7:
                card = new BasicButtonsCard(this);
                card.setDescription("This programme provides a combination of the study of IT with business modules designed to enable you to exploit your technical innovations commercially...");
                card.setTitle("Bachelor of Science (Hons) in Technopreneurship");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

               if(position % 2 == 0) {
                   (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                   (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                   ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                   ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
               } else {
                   (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                   (card).setTitleColorRes(R.color.custom_card_bg1);
                   ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                   (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                   ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
               }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section8)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_technopreneurship"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 8:
                card = new BasicButtonsCard(this);
                card.setDescription("The aims, objectives and learning outcomes of the programme are in line with and supportive of, the vision and mission of the HEP by producing ...");
                card.setTitle("Bachelor of Science (Hons)in Instructional Technology");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }

                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section9)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_instructional_technology"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            case 9:
                card = new BasicButtonsCard(this);
                card.setDescription("This programme will direct students to the development and use of multimedia systems. Students are exposed and trained in the relevant ...");
                card.setTitle("Bachelor of Science (Hons) in Software Engineering with Multimedia");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");

                if(position % 2 == 0) {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg1));
                    (card).setTitleColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.fbutton_color_asbestos);
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);
                } else {
                    (card).setBackgroundColor(getResources().getColor(R.color.custom_card_bg2));
                    (card).setTitleColorRes(R.color.custom_card_bg1);
                    ((BasicButtonsCard) card).setLeftButtonTextColorRes(R.color.custom_card_bg1);
                    (card).setDescriptionColor(getResources().getColor(R.color.custom_card_txt));
                    ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_light);
                }
                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title("Limkokwing University")
                                .content(R.string.section10)
                                .positiveText("DONE")
                                .show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net/m/courses_details/bachelor_of_science_hons_in_software_engineering_with_multimedia"));
                        startActivity(page2);
                    }
                });
                card.setDismissible(false);

                return card;

            default:
                card = new BasicButtonsCard(this);
                card.setDescription("");
                card.setTitle("");
                card.setTag("BASIC_BUTTONS_CARD");
                ((BasicButtonsCard) card).setLeftButtonText("MORE INFO...");
                ((BasicButtonsCard) card).setRightButtonText("OFFICIAL PAGE");
                ((BasicButtonsCard) card).setRightButtonTextColorRes(R.color.accent_material_dark);


                ((BasicButtonsCard) card).setDividerVisible(true);

                ((BasicButtonsCard) card).setOnLeftButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Toast.makeText(mContext, "You have pressed the left button", Toast.LENGTH_SHORT).show();
                    }
                });

                ((BasicButtonsCard) card).setOnRightButtonPressedListener(new OnButtonPressListener() {
                    @Override
                    public void onButtonPressedListener(View view, Card card) {
                        Toast.makeText(mContext, "You have pressed the right button", Toast.LENGTH_SHORT).show();
                    }
                });
                card.setDismissible(false);

                return card;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(MainActivity.this,Limkokwing.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}