package com.vm.limkokwing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;

import at.markushi.ui.CircleButton;


public class Contact_us extends ActionBarActivity {

    CircleButton contact,email,twitter,website;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        contact = (CircleButton)findViewById(R.id.contact_but);
        email = (CircleButton)findViewById(R.id.email_but);
        twitter = (CircleButton)findViewById(R.id.twitter_but);
        website = (CircleButton)findViewById(R.id.website);

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "0383178888"));
                startActivity(intent);
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "info@limkokwing.net", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Contacting Through App");
                startActivity(Intent.createChooser(intent, "Send email..."));
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=limkokwing_my"));
                        startActivity(intent);

                    }catch (Exception e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/limkokwing_my")));
                    }
            }
        });

        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent page2 = new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.limkokwing.net"));
                startActivity(page2);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(Contact_us.this,Limkokwing.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}